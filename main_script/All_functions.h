// -----------------------------------------------------------------------
// Use shortest (memory wise) variable definition possible and seperate out sections 

// LIBRARIES to IMPORT
#include <Arduino_LSM6DS3.h> 
#include <SimpleKalmanFilter.h>
#include <Adafruit_MotorShield.h> 
#include <Servo.h> 
#include "utility/Adafruit_MS_PWMServoDriver.h"
#include <Wire.h>

SimpleKalmanFilter pf = SimpleKalmanFilter(0.5, 2, 0.01); // these filters are for the IMU
SimpleKalmanFilter rf = SimpleKalmanFilter(0.5, 2, 0.01); // the library reduces the noise in the signal by
SimpleKalmanFilter yf = SimpleKalmanFilter(0.5, 2, 0.01); // predicting the next value

Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 
Adafruit_DCMotor *leftMotor = AFMS.getMotor(3); // the motors are assigned to the outputs on the arduino
Adafruit_DCMotor *rightMotor = AFMS.getMotor(4); 
Adafruit_DCMotor *pitchMotor = AFMS.getMotor(2);

Servo grabber_servo;

// PINMAP DEFINITION ---------------------------------------------------------
// Left Motor = Motor 1 
// Right Motor = Motor 2
// Pitch Motor = Motor 3
int leftEncoder; 
int rightEncoder;
int high_microswitch_pin; 
int low_microswitch_pin;
int echoForwardTrigPin = 13;
int echoLeftTrigPin = 7; 
int echoForwardPin = 8;
int echoLeftPin = 6;
int greenLED = 3;
int redLED = 4;
int IR_38kHz; 
int irdist; 

// ----- ENCODER VARIABLES -----------------------------------------------

float ans;                             // for return
float leftPulseWidth;                  // in us
float rightPulseWidth;                 // in us
long leftForwardDisplacement = 0;      // in mm
long leftSidewayDisplacement = 0;      // in mm
long rightForwardDisplacement = 0;     // in mm
long rightSidewayDisplacement = 0;     // in mm
long forwardDisplacement = 0;           // in mm
long sidewayDisplacement = 0;       // in mm, left is the positive direction
float orientation = 0;              // in radian, positive when turning left, negative when turning right
float radius;                 // in mm,  define the redius of the wheel here
float d;                         //  in mm, define the distance between two wheels
float leftWheelSpeed;               // in mm/s
float rightWheelSpeed;              // in mm/s
float speedDifference;              // in mm/s
float omega;                        //  radian/s, turning angular velocity
bool state;                         // true when going forward, false when going backward
float distance_travelled;
int tl;
int tr;
#define leftTransistor  A0
#define rightTransistor  A1

// ----- IMU VARIABLES -----------------------------------------------------

float x = 0;
float y = 0; 
float z = 0; 

// ----- MOTOR CONTROLLER VARIABLES -----------------------------------------




// SETUP OF THE PINS //
void pinSetup(){
// this function sets the I/O of the pins for the encoder, microswitches and echo sensors

    pinMode(leftEncoder, INPUT);
    pinMode(rightEncoder, INPUT);
    pinMode(high_microswitch_pin, INPUT);
    pinMode(low_microswitch_pin, INPUT);
    pinMode(echoForwardTrigPin, OUTPUT);
    pinMode(echoLeftPin, INPUT);
    pinMode(echoForwardPin, INPUT);
    pinMode(echoLeftTrigPin, OUTPUT);
    pinMode(echoForwardTrigPin, OUTPUT);
    pinMode(leftTransistor,INPUT);
    pinMode(rightTransistor,INPUT);
    pinMode(greenLED, OUTPUT);
    pinMode(redLED, OUTPUT);
}
// FLASHING LEDS!!!//
void flashGreen(){
  // this function turns on and off the green led, as is required when a blue dummy is found
    digitalWrite(greenLED, HIGH);
    digitalWrite(redLED,LOW);
    delay(250);
    digitalWrite(greenLED,LOW);
    delay(250);
    digitalWrite(redLED,LOW);
    delay(250);
    digitalWrite(greenLED,LOW);
    delay(250);
    digitalWrite(redLED,LOW);
    delay(250);
    digitalWrite(greenLED,LOW);
    delay(250);
    digitalWrite(redLED,LOW);
    delay(250);
    digitalWrite(greenLED,LOW);
    delay(250);
    digitalWrite(redLED,LOW);
    delay(250);
    digitalWrite(greenLED,LOW);
    delay(250);
}
void flashRed(){
  // this function flashes the red LED, as is required when a red dummy is found
  digitalWrite(greenLED,LOW);
  digitalWrite(redLED, HIGH);
  delay(250);
  digitalWrite(redLED,LOW);
  delay(250);
  digitalWrite(redLED, HIGH);
  delay(250);
  digitalWrite(redLED,LOW);
  delay(250);
  digitalWrite(redLED, HIGH);
  delay(250);
  digitalWrite(redLED,LOW);
  delay(250);
  digitalWrite(redLED, HIGH);
  delay(250);
  digitalWrite(redLED,LOW);
  delay(250);
  digitalWrite(redLED, HIGH);
  delay(250);
  digitalWrite(redLED,LOW);
  delay(250);  
}
void flashBoth(){
  // this function flashes the red and green leds, as is required when a white dummy is found
  digitalWrite(redLED, HIGH);
  digitalWrite(greenLED,HIGH);
  delay(250);
  digitalWrite(redLED,LOW);
  digitalWrite(greenLED,LOW);
  delay(250);
  digitalWrite(redLED, HIGH);
  digitalWrite(greenLED,HIGH);
  delay(250);
  digitalWrite(redLED,LOW);
  digitalWrite(greenLED,LOW);
  delay(250);
  digitalWrite(redLED, HIGH);
  digitalWrite(greenLED,HIGH);
  delay(250);
  digitalWrite(redLED,LOW);
  digitalWrite(greenLED,LOW);
  delay(250);
  digitalWrite(redLED, HIGH);
  digitalWrite(greenLED,HIGH);
  delay(250);
  digitalWrite(redLED,LOW);
  digitalWrite(greenLED,LOW);
  delay(250);
  digitalWrite(redLED, HIGH);
  digitalWrite(greenLED,HIGH);
  delay(250);
  digitalWrite(redLED,LOW);
  digitalWrite(greenLED,LOW);
  delay(250);
}
// --- Maths Functions ---- //

float calculateRemainder(float numerator, float demoninator){
// this function returns the remainder that is left after repeated addition
// this function is the same as % however it also works with floats
// it uses repeated subtraction
  float temp;
  temp = numerator;
  while (temp > demoninator){
    temp = temp - demoninator;
  }
  return temp;
}
// This function return the analogRead of photo transistor.
// We have two photo transistors originally, but one of them is broken. 
int readLeftTransistor(){
  tl = analogRead(leftTransistor);
  return tl;
}
int readRightTransistor(){
  tr = analogRead(rightTransistor);
  return tr;
}
// It returns the average readings of the transistor readings
long averageLeftTransistor(int number){
  int readings[number];
  long leftAverage = 0;
  for (int i = 0 ; i<number ; i++){
      readings[i] = readLeftTransistor();
      leftAverage += readings[i];
  }
  leftAverage = leftAverage/number;
  return leftAverage;
}
int averageRightTransistor(int number){
  int readings[number];
  int rightAverage = 0;
  for (int i = 0 ; i<number ; i++){
      readings[i] = readRightTransistor();
      rightAverage += readings[i];
  }
  rightAverage = rightAverage/number;
  return rightAverage;
}
// IMU FUNCTIONS ------------------------------------------------------------------
float getIMUData(){ 
  // this function returns the angular velocity of the roobt in degrees per second
  // for each of the three directions, in order of roll, pitch, yaw
    if (IMU.gyroscopeAvailable()) {
    IMU.readGyroscope(x, y, z);
    // this uses the Kalman filters to make an estiamte for each of the directions
    float estimated_roll = rf.updateEstimate(x); 
    float estimated_pitch = pf.updateEstimate(y);
    float estimated_yaw = yf.updateEstimate(z);

    return estimated_roll, estimated_pitch, estimated_yaw;
}
}

// SERVO (only grabber) CONTROL -----------------------------------------------------------

void open_grabber(){ 
  // this function sets the servo to the open position
  // the values found here have been tested and input manually
    grabber_servo.write(50);
}

void close_grabber(){
  // this function sets the servo to the closed position
  // the values found here have been tested and input manually
    grabber_servo.write(140);
}

// MOTOR CONTROL FUNCTIONS --------------------------------------------------------
// - default direction is true, speed is in percentage form

void left_Motor_Control(bool direction = true, int speed = 100){ 
  // this fucntion sets the motor to the given throttle (between 0 and 100)
  // this function also sets the direction of the motor
    if (speed > 100){ // this if statement ensures that the inputted speed is within the expected range
      speed = 100;
    }
    if (speed < 0){
      speed = 0;
    }
    leftMotor->setSpeed(speed * 2.55); // the expected value for the motors is a 8 bit number, (i.e. up to 255)
    if (direction == false){
      leftMotor->run(FORWARD);
    }
    else {
      leftMotor->run(BACKWARD);
    }
}

void right_Motor_Control(bool direction = true, int speed = 100){ 
  // this function is identicla to the left_Motor_Control function, see above
    if (speed > 100){
      speed = 100;
    }
    if (speed < 0){
      speed = 0;
    }
    rightMotor->setSpeed(speed * 2.55);
    if (direction == false)
    {rightMotor->run(FORWARD);}
    else 
    {rightMotor->run(BACKWARD);}
}
void pitchControl(bool direction = true, int speed = 100){ 
  // this function is identical to the left_Motor_Control function, see above
    if (speed > 100){
      speed = 100;
    }
    if (speed < 0){
      speed = 0;
    }
    pitchMotor->setSpeed(speed * 2.55);
    if (direction == true)
    {pitchMotor->run(FORWARD);}
    else 
    {pitchMotor->run(BACKWARD);}
}
// Lift the grabber up and down
void pitchUp(){
  // this function sets the pitch motor in the forwards direction for a predetermined amount of time
  // this ensures that the grabber is in the up position
  pitchControl(true, 100);
  delay(3500);
  pitchControl(true, 0);
}

void pitchDown(){
  // this function sets the pitch motor in the backwards direction for a predetermined amount of time
  // this ensures that the grabber is in the down position
  pitchControl(false, 100);
  delay(1500);
  pitchControl(false, 0);
}
// It will get the dummy in front of it
void getDummy(){
  // this function uses the above functions to get the dummy
  // it does this by pitching down, closing the grabber (around the dummy) and lifting it up
  pitchDown();
  delay(500);
  close_grabber();
  delay(500);
  pitchUp();
  delay(500);
}
// It will drop the dummy in front of it and adjust the position of grabber after dropping
void dropDummy(){
  // this fuction uses the above functions to put the dummy down
  // it does this by pitching down, releasing the grabber and pithcing back up again
  pitchDown();
  delay(1000);
  open_grabber();
  pitchDown();
  delay(1000);
  pitchUp();
}
            



// It will go for a certain amount of time
void go(bool direction, int speed, int duration){
  // This function takes the direction (true = forward), a throttle between 0 and 100
  // and a duration measured in milliseconds, then sets the motors to this for that duration.

  left_Motor_Control(direction, speed);
  right_Motor_Control(direction, speed);

  delay(duration);

  left_Motor_Control(direction, 0);
  right_Motor_Control(direction, 0);
}


// Turn using IMU (proportional control)
void turn(bool direction = true, int angle = 90, int baseSpeed = 0) {
// Target angle in degrees 
// Try to veriify turning using encoders as well if required? What is the IMU turning accuracy? 
// TRUE DIRECTION IS CLOCKWISE from a birds-eye view of the robot
    
    float wide_sum = 0.0;
    float kp = 2; 
    float yawVel = 0; 
    float pitchVel = 0; 
    float rollVel = 0;
    float delta_t = 0;
    float time_new = 0; 
    float targetAngle = angle * 1.07;
    int safety_timer = millis();
    float offset;
    float local_sum;

    if (direction){ // these offests have been found manaully by testing
      offset = targetAngle * (-0.18);
    }
    else { // the offsets are best tuned for 90 degree turns, thus it may be better to turn 90 degrees twice for a 180 degree turn
      offset = targetAngle * (-0.15);
    }

    while (wide_sum < ((targetAngle + offset))*1000000){ // the turn continues until the integral exceeds the target angle
          time_new = micros();  // the time is saved

          pitchVel, rollVel, yawVel = getIMUData();
          if (yawVel < 0){
            yawVel = yawVel * -1; // the yaw velocity is made to be positive
          }
  
          float error = targetAngle - wide_sum; // the error is calculated. the variable wide_sum is the integral
          float motor_speed = error * kp + baseSpeed; // the motor speeds are directio proportional to the erorr. The baseSpeed is used to overcome the rolling resistance
          left_Motor_Control(direction, abs(motor_speed)); // directino = true implies that the turn will be clockwise, thus the
          right_Motor_Control(!direction, abs(motor_speed)); // motors are set in opposiing diretion as requried
          
          delta_t = micros() - time_new; // the time taken since the time was saved is calculated in microseconds
          local_sum = yawVel * delta_t; // the angular displacement over the last iteration is calculated
          wide_sum += local_sum; // the integral includes this last segment
    }
    left_Motor_Control(true, 0); // the motors are turned off after the turn is completed
    right_Motor_Control(true, 0);
}
// turn more accurately
void turnBetter(bool direction, int angle){
  float aim; // this function includes additional offsets that prove to be more accurate, as well as removing the need for the input of a base speed
  if (direction == true){
    aim = angle * 1.009;
  }
  else{
    aim = angle * 0.85;
  }
  turn(direction, aim,50);
}

// ULTRASONIC SENSOR -----------------------------------------------------------
// Return the distance forward in mm
int ultrasonicDistanceForward(){
    
    long duration = 0;
    int ultrasonic_distance = 0;
    
    digitalWrite(echoForwardTrigPin, LOW); // the trigger pin is set to high for 10 microseconds
    delayMicroseconds(10);    
    digitalWrite(echoForwardTrigPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(echoForwardTrigPin, LOW);
    
    duration = pulseIn(echoForwardPin, HIGH); // the duration of the pulse in is measured
    // returns in mm 
    ultrasonic_distance = 10 * (duration*0.0343)/2; // the duration is proportional to the distance via the speed of sound thus the distance can be calculated
    
    return ultrasonic_distance;
}
// Return the left distance in mm
int ultrasonicDistanceLeft(){
  // this function is identical to the ultrasonicDistanceForward, only it used different pins
  // such that it is to do with the left ultrasoinc sensor
    
    long duration = 0;
    int ultrasonic_distance = 0;
    
    
    digitalWrite(echoLeftTrigPin, LOW);
    delayMicroseconds(10);    
    digitalWrite(echoLeftTrigPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(echoLeftTrigPin, LOW);
    
    duration = pulseIn(echoLeftPin, HIGH);
    // returns in mm 
    ultrasonic_distance = 10*(duration*0.0343)/2;
    
    return ultrasonic_distance;
}

// Dummy identify ----------------------------------------------------------- 

int identify(){
   // Initialise array that holds pulse width values obtained from 38Khz sensor 
   int sensor_array[10];
   
   // Initialise variables used for maths operations 
   double sum = 0; 
   double array_len = 10; 
   double average;
   
   // Store data in array. Perhaps 1000 is too large, time data collection duration
   for (int i = 0; i < 10; i++){
    sensor_array[i] = pulseIn(A0, HIGH);
   }

   // Perform mathematical operations on sensor_array
   for (int j = 0; j < array_len; j++){
    sum += sensor_array[j]; 
   }

   average = sum / array_len; 

   // CHANGE the specific values based on more data collection using a Picoscope. 
   if (average <= 50){ 
    // This is the white dummy
    return 1; 
   }

   if (average > 50 & average < 550){
    // This is the blue dummy
    return 3; 
   }

   if (average > 570){ 
    // This is the red dummy 
    return 2;
   }

   // This may need to be modified! Should we rerun the function to try get good 
   // values? Do we adjust sensor orientation before getting another set of data? 
   // Needs to be decided after experimentation and observation of failure rate of 
   // this decision algorithmn.
    
   else { 
    return 4; 
    // The 4 corresponds to an error (garbled data). Modify robot position if possible to
    // re-align the sensor. 
   }
}
