// LIBRARIES TO IMPORT -------------------------------------------------------- 
#include "All_functions.h"
#include "line_sensor.h"
#include "travel.h"
void setup() { 

    Serial.begin(9600);
    while (!Serial);
    
    // IMU SETUP ------------------------------------------------------------   

    if (!IMU.begin()) {
        Serial.println("Failed to initialize IMU!");
        while (1);
    }

    Serial.print("Gyroscope sample rate = ");
    Serial.print(IMU.gyroscopeSampleRate());
    Serial.println(" Hz");
    Serial.println();
    Serial.println("Gyroscope in degrees/second");
    Serial.println("X\tY\tZ");


    // MOTOR CONTROL SETUP -----------------------------------------------------

    //if (!AFMS.begin()) {         // create with the default frequency 1.6KHz
    // if (!AFMS.begin(1000)) {  // OR with a different frequency, say 1KHz
    //    Serial.println("Could not find Motor Shield. Check wiring.");
    //    while (1);

    AFMS.begin();

    grabber_servo.attach(9);

    pinSetup();
}
/*void loop(){
  followLine(100,1000);
  delay(1000);
} */
void loop(){
  open_grabber();                                               // Adjust the grabber at an open position so that it won't touch the dummy
  int dist =  ultrasonicDistanceForward();                      // Go until 5cm from the dummy on the line
  while(dist > 50){
    if (readLeftLightSensor() && readRightLightSensor()){
      go(true,100, 10);
    }
    else{
      followLine(100,10);
    }
    dist = ultrasonicDistanceForward();
    while(dist < 30){
      dist = ultrasonicDistanceForward();
    }
  }
  go(true,0,100);                                               // Stop after finding the dummy
  homeOnDummySuperShort();                                      // Find the signal(identify more accurately)
  int identification1;
  identification1 = identify();
  delay(2000);
  if(identification1 ==4){                                      // If there is an error, consider this dummy as the white dummy
    identification1 = 1;
  }
  if (identification1 == 1){                                    // White
    flashBoth();
  }
  else if(identification1 == 2){                                // Red
    flashRed();
  }
  else if(identification1 == 3){                                // Blue
    flashGreen();
  }
  getDummy();
  if (identification1 == 1){
    goWhite();
    delay(1000);
    dropDummy();
    backFromWhite();
  }
  else if(identification1 == 2){
    turnBetter(true,180);
    goRed();
    delay(1000);
    dropDummy();
    backFromRed();
  }
  else if(identification1 ==3){
    turnBetter(true,180);
    goBlue();
    delay(1000);
    dropDummy();
    backFromBlue();
  }
  delay(500);
  turnBetter(true,180);                                         // Turn around and start to get the second dummy
  pitchDown();                                                  // Adjust the position of photo transistor so that it can receive the IR signals
  followLine(100, 100000);                                      // Follow the line until the intersection
  turnBetter(true, 180);                                        // Turn around
  go(true,100,2000);                                            // Go to the centre of the search zone
  turnBetter(false,50);                                         // Turn and start scanning
  getToDummy();                                                 // Find and get to the dummy
  int identification3 = identify();                             // Identify as above
  if (identification3 ==4){
    identification3 == 2;
  }
  if (identification3 == 1){
    flashBoth();
  }
  else if(identification3 == 2){
    flashRed();
  }
  else if(identification3 == 3){
    flashGreen();
  }
  getDummy();
  turnBetter(true,180);
  backToLine(100,false);
  if (identification3 == 1){
    turnBetter(true,180);
    goWhite();
    dropDummy();
    backFromWhite();
  }
  else if(identification3 == 2){
    goRed();
    dropDummy();
    backFromRed();
  }
  else if(identification3 ==3){
    goBlue();
    dropDummy();
    backFromBlue();
  }
  turnBetter(true,180);                                           // Start to get the third dummy
  pitchDown();                                                    // Adjust the position of photo transistor
  followLine(100,100000);                                         // Follow the line until the intersection                      
  turnBetter(false, 180);                                         // Turn around
  go(true,100,2000);                                              // Go to the centre of search zone
  turn(true,145);                                                 // Turn and start scanning
  getToDummy();                                                   // Get to the dummy
  int identification2 = identify();                               // Identify
  while(identification2 ==4){
    identification2 == identify();                                
  }
  if (identification2 == 1){
    flashBoth();
  }
  else if(identification2 == 2){
    flashRed();
  }
  else if(identification2 == 3){
    flashGreen();
  }
  getDummy();
  turnBetter(true,150);
  backToLine(100,true);
  if (identification2 == 1){
    turnBetter(true,180);
    goWhite();
    dropDummy();
    backFromWhite();
  }
  else if(identification2 == 2){
    goRed();
    dropDummy();
    backFromRed();
  }
  else if(identification2 ==3){
    goBlue();
    dropDummy();
    backFromBlue();
  }
  backToStart();                                                // Back to the start zone after getting three dummies
}
void goWhite(){                                                 // Go to the white square
  followLine(100,30000);
}
void backFromWhite(){                                           // Go back to the first junction after dropping down the dummy
  go(false,100,1000);
  turnBetter(true,180);
  followLine(100,60000);
}
void backToStart(){                                             // Go back to the start zone from the first intersection
  go(true,100,300);
  followLine(100,10000000);
  go(true,100,3000);
  turnBetter(false,180);
  delay(10000000);
}
void goRed(){                                                   // Go to red square
  followLine(100,70000);
  go(true,100,1200);
  turnBetter(true,90);
  go(true,100,500);
}
void backFromRed(){                                             // Back to the first intersection from the red square
  go(false,100,500);
  turnBetter(false,90);
}
void goBlue(){                                                  // Go to the blue square
  followLine(100,10000000);
  go(true,100,1200);
  turnBetter(false,90);
  go(true,100,500);
}
void backFromBlue(){                                            // Go back to the first intersection from blue square
  go(false,100,500);
  turnBetter(true,90);
}
