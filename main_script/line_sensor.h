#include <Arduino_LSM6DS3.h> 
int leftSensor = 0;
int rightSensor = 2;
int backSensor = 4;
bool intersection;
bool onLine;
// This followLine() function will followLine for a period unless it meets an intersection.
void followLine(int speed, int time) {// time in ms
  // the robot will break out of the loop when an intersection is detected
  int startTime = millis();
  int left;
  int right;
  int back;

  while (millis() < startTime + time){
  // this loop repeats until the timer runs out //

    left = digitalRead(leftSensor);
    back = digitalRead(backSensor);
    right = digitalRead(rightSensor);

    // case 1: on the line
    if (left == LOW && back == HIGH && right == LOW){
      onLine = true;
      right_Motor_Control(true,speed);
      left_Motor_Control(true, speed); 
    }

    // case 2 : pointing right
    else if (left == HIGH && right == LOW){
      onLine = true;
      right_Motor_Control(true,speed);
      left_Motor_Control(true,speed * 0.2);
    }
    
    // case 3 : pointing left
    else if (left == LOW && right == HIGH){
      right_Motor_Control(true,speed * 0.2);
      left_Motor_Control(true,speed);
    }
    // case 4: one the intersection
    else if (left == HIGH && right == HIGH){
      right_Motor_Control(true,0);
      left_Motor_Control(true,0);
      break;
    }
    else{
      // case 5: off the line
      right_Motor_Control(true,speed);
      left_Motor_Control(true,speed);
      onLine = false;
      intersection = false;
    }
  }
}
// This followLineIgnore function will follow line for a period and ignore any intersection.
void followLineIgnore(int speed, int time){
  // the robot will break out of the loop when an intersection is detected
  int startTime = millis();
  int left;
  int right;
  int back;

  while (millis() < startTime + time){
  // this loop repeats until the timer runs out //

    left = digitalRead(leftSensor);
    back = digitalRead(backSensor);
    right = digitalRead(rightSensor);

    // case 1: on the line
    if (left == LOW && back == HIGH && right == LOW){
      onLine = true;
      right_Motor_Control(true,speed);
      left_Motor_Control(true, speed); 
    }

    // case 2 : pointing right
    else if (left == HIGH && right == LOW){
      onLine = true;
      right_Motor_Control(true,speed);
      left_Motor_Control(true,speed * 0.2);
    }
    
    // case 3 : pointing left
    else if (left == LOW && right == HIGH){
      right_Motor_Control(true,speed * 0.2);
      left_Motor_Control(true,speed);
    }
    // case 4: one the intersection
    else if (left == HIGH && right == HIGH){
    }
    else{
      // case 5: off the line
      right_Motor_Control(true,speed);
      left_Motor_Control(true,speed);
      onLine = false;
      intersection = false;
    }
  }
}
bool readLightSensors(){
  // probably don't use this function //
  return digitalRead(leftSensor) , digitalRead(backSensor), digitalRead(rightSensor);
}
bool readLeftLightSensor(){
  return digitalRead(leftSensor);
}
bool readRightLightSensor(){
  return digitalRead(rightSensor);
}
bool readBackLightSensor(){
  return digitalRead(backSensor);
}
// The robot will go back to line at any angle to the line
void backToLine(int speed, bool direction){
  // this function returns the robot to the line. The direction dictates which way it will turn when it reaches
  // the line, where direction = true is clockwise

  // this loop drives the robot forwards until the robot reaches the line
  while (readLeftLightSensor() == LOW && readRightLightSensor() == LOW){
    left_Motor_Control(true, speed);
    right_Motor_Control(true, speed);
    delay(10);
  }
  left_Motor_Control(true,0);
  right_Motor_Control(true,0);
  // this moves the robot further over the line, such that the centre of rotation roughly on the line
  go(true, 100, 1200);


  // these loops turn the robot until the front light sensors are on the line, implying that it is facing in the right
  // direction (with a small but correctable offset, that the followLine function can handle)
  if (direction){
    left_Motor_Control(true, speed);
    right_Motor_Control(false, speed);
    while (readLeftLightSensor() == LOW){
      delay(10);
    }
  }
  else{
    left_Motor_Control(false, speed);
    right_Motor_Control(true, speed);
    while (readRightLightSensor() == LOW){
      delay(10);
    }
  }
  left_Motor_Control(true,0);
  right_Motor_Control(true,0);
}
