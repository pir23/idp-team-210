// This library contains all of the functions required to move the robot in different ways.
#include <SimpleKalmanFilter.h>
SimpleKalmanFilter ultrafor = SimpleKalmanFilter(1.5, 1.5, 0.05);

// Detect the continuity using the left sensor, if the readings from the left ultrasonic sensor are continious, return true.
bool continuity(float distances[5]){
    float average; 
    average = 0.0;
    for (int i = 0 ; i<5 ; i++){
        average += distances[i]; // this calculates the average of the inputted list
    }
    average = average * 0.2;
    if (average > 0){

    bool result = true;
    for (int i = 0 ; i<5 ; i++){
        if (abs(distances[i] - average) > 30){        //Define continious here (every value to within 30 of the average)
            result = false;
        }
    }
    if (result){
      Serial.println("I found one!");
    }
    return result;
    }
    else {
      return false;
    }
    
}
// It will follow the line untill several intersections
void followLineToEnd(int speed, int intersectionCount){
  while (intersectionCount  > 0){
    if (readLeftLightSensor() && readRightLightSensor()){ // if both of the front sensors are true, this means we must be at an intersection
      intersectionCount--;
      left_Motor_Control(true,100); // the robot must go forwards blindly to ensure it does not double count the same intersection
      right_Motor_Control(true,100);
      delay(250);
    }
    else{
      followLine(100,10); // the robot will follow the line for a short amount of time before checking again
    }
  }
  while (intersectionCount == 0){
    followLine(100,10);
    if (readLeftLightSensor() && readRightLightSensor()){
      break; // if the right number of intersections has been counted, the loop will break
    }
  }
  delay(1000);
}
// It will follow the line until the front ultrasonic sensor detects it is certain distant to a boundary.
void followLineUntilForward(int speed, int dist){
  int test_dist = 100000;
  while (test_dist > dist){
    test_dist = ultrasonicDistanceForward();
    followLine(100,10);
    if (readLeftLightSensor() && readRightLightSensor()){
      right_Motor_Control(true, 100); // as before, the robot must pass over intersections blindly else it will pause
      left_Motor_Control(true, 100);
      delay(200);
      right_Motor_Control(true, 100);
      left_Motor_Control(true, 100);
    }
    while (test_dist < 10){
      test_dist = ultrasonicDistanceForward(); // this loop is to ensure that there are no erroneous values
      delay(5);
    }
    Serial.println(test_dist);
  }
  left_Motor_Control(true,0);
  right_Motor_Control(true,0);
}
// It will follow the line untill several intersections detected or continuity detected.
void followLineUntilContinuity(int speed, int time, int intersectionsUntilTurn){ // input the time required to follow the line for //
    // This function will follow the line for the time milliseconds using the line sensor
    // This function will stop when a discontinuity is detected i.e. a dummy is detected
    int rollingAverageNum = 5; // this variable sets the length of the list of values stored
    float distances[5];
    for (int i = 0 ; i<rollingAverageNum ; i++){
      distances[i] = -100000; // initially, all the values are highly negative to signal that the list has not been populated yet
    }
    int oldestValue = 4; // the last value in the list is considered the oldest
    
    int startTime;
    startTime = millis();

    while (millis() < startTime + time){ // a safety timer is included
        followLine(speed, 3);
        //delay(3);
        if (readBackLightSensor() && (readLeftLightSensor() && readRightLightSensor())){
            if (intersectionsUntilTurn == 0){ // this function also detects and counts intersections
                left_Motor_Control(true, 0);
                right_Motor_Control(true, 0);
                break;
            }
            else{
                intersectionsUntilTurn --;
                //left_Motor_Control(true, 0);
                //right_Motor_Control(true, 0);
                //delay(500);
                left_Motor_Control(true, speed);
                right_Motor_Control(true, speed);
                while (readBackLightSensor() && (readLeftLightSensor() && readRightLightSensor())){
                  ;
                }
                left_Motor_Control(true, 0);
                right_Motor_Control(true, 0);
            }
        }
        distances[oldestValue] = ultrasonicDistanceLeft(); // this replaces the oldest value with the curret value
        oldestValue --; // this increments the oldest value such that the new oldest will be replaced next time
        if (oldestValue < 0){
            oldestValue = rollingAverageNum - 1; // if the oldest value was item 0 in the list, it is now the final item in the list
        }
        if (continuity(distances)) // the list of distances is tested for continuity
        {
          left_Motor_Control(true, 0); // if a continuity is found, the robot stops and breaks out of the loop
          right_Motor_Control(true, 0);  
          break;
        }
        else{
          ; // otherwise, the robot continues to gather values and to folllow the line
        }
    }
    left_Motor_Control(true, 0);
    right_Motor_Control(true, 0);
    // it is expected that after this function has been run that the robot will turn towards the dummy i.e. a anticlockwise 90 degree turn //
}
// These homing function will home the dummy using the photo transistor
void homeOnDummyLong(){
  int leftReadingNew = averageLeftTransistor(500);
  int leftReadingOld = 0;
  turn(true,20);
  while (leftReadingNew > leftReadingOld || (leftReadingNew < 4)){ // this loop continues until the values given by the transistor begin to go down
    leftReadingOld = leftReadingNew;
    leftReadingNew = averageLeftTransistor(500);
    Serial.println(leftReadingNew);
    left_Motor_Control(false,30);
    right_Motor_Control(true,30);
  }
  left_Motor_Control(true, 0);
  right_Motor_Control(true, 0);
}
void homeOnDummyMid(){
  int leftReadingNew = averageLeftTransistor(500);
  int leftReadingOld = 0;
  turn(true,20);
  while (leftReadingNew > leftReadingOld || (leftReadingNew < 20)){
    leftReadingOld = leftReadingNew;
    leftReadingNew = averageLeftTransistor(500);
    Serial.println(leftReadingNew);
    left_Motor_Control(false,30);
    right_Motor_Control(true,30);
  }
  left_Motor_Control(true, 0);
  right_Motor_Control(true, 0);
}
void homeOnDummyShort(){
  // same as the other homeOnDummy functions, but with different critical values
  int leftReadingNew = averageLeftTransistor(500);
  int leftReadingOld = 0;
  turn(true,20);
  while (leftReadingNew > leftReadingOld || (leftReadingNew < 40)){
    leftReadingOld = leftReadingNew;
    leftReadingNew = averageLeftTransistor(500);
    Serial.println(leftReadingNew);
    left_Motor_Control(false,30);
    right_Motor_Control(true,30);
  }
  left_Motor_Control(true, 0);
  right_Motor_Control(true, 0);
}
//This homeOnDummySuperShort() will home on dummy when 5 cm to the dummy, ensuring that the photo transistor can get a strong signal.
void homeOnDummySuperShort(){
  int leftReadingNew = averageLeftTransistor(500);
  int leftReadingOld = 0;
  turn(true,5);
  while (leftReadingNew > leftReadingOld || (leftReadingNew < 50)){
    leftReadingOld = leftReadingNew;
    leftReadingNew = averageLeftTransistor(500);
    Serial.println(leftReadingNew);
    left_Motor_Control(false,30);
    right_Motor_Control(true,30);
  }
  left_Motor_Control(true, 0);
  right_Motor_Control(true, 0);
}
// It will approach the dummy by doing homing three times
// Home ---> move some distance ----->Home-----> move ----->home and move------>home from super short distance
void getToDummy(){
  open_grabber();
  int dist = ultrasonicDistanceForward();
  homeOnDummyLong();
  go(true,80,dist*5);
  dist = ultrasonicDistanceForward();
  homeOnDummyMid();
  go(true,80,(dist-50)*5);
  homeOnDummyShort();
  while (dist > 50){
    left_Motor_Control(true,50);
    right_Motor_Control(true,50);
    dist = ultrasonicDistanceForward();
    while (dist < 10){
      dist = ultrasonicDistanceForward();
    }
  }
  left_Motor_Control(true, 0);
  right_Motor_Control(true, 0);
  homeOnDummySuperShort();
  }
  
