// This sketch takes pin 0 as left encoder input, pin 1 as right encoder input. 
// The radius of the wheel (assuming that both have the same size) and the distance between two wheels
// are to be defined.
// It returns leftWheelSpeed, rightWheelSpeed, orientation, forwardDisplacement and sidewayDisplacement.
// encoder(1) gives leftWheelSpeed
// encoder(2) gives rightWheelSpeed
// encoder(3) gives orientation
// encoder(4) gives forwardDisplacement
// encoder(5) gives sidewayDisplacement

// IMPOTTANT! There is a set up function in this .h file which should be deleted. It is just a reminder that set pin0 and 1 as attachinterrupt pin.

#define leftEncoder 0
#define rightEncoder 1
float ans;
volatile int leftPrevious = 0;         // store the previous time
volatile int rightPrevious = 0;
volatile int leftPulseWidth;           // in us
volatile int rightPulseWidth;          // in us
volatile long leftForwardDisplacement = 0;      // in mm
volatile long leftSidewayDisplavement = 0;      // in mm
volatile long rightForwardDisplacement = 0;     // in mm
volatile long rightSidewayDisplavement = 0;     // in mm
long forwardDisplacement = 0;           // in mm
long sidewayDisplacement = 0;       // in mm, left is the positive direction
volatile float orientation = 0;   // in radian, positive when turning left, negative when turning right
float radius =    ;      // in mm,  define the redius of the wheel here
float d = ;             //  in mm, define the distance between two wheels
volatile float leftWheelSpeed;   // in mm/s
volatile float rightWheelSpeed;  // in mm/s
volatile float speedDifference;  // in mm/s
volatile float omega;               //  radian/s, turning angular velocity   
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  attachInterrupt(leftEncoder,leftTimeDifference,CHANGE);         // Once the encoder goes from HIGH to LOW or LOW to HIGH, it will interrupt
  attachInterrupt(rightEncoder,rightTimeDifference,CHANGE);
}
void leftTimeDifference(){
  leftPulseWidth = micros() - leftPrevious;
  leftPrevious = micros();
  if (speed > 0){
    leftWheelSpeed = 1000000 * (radius * PI / 72)/leftPulseWidth;   // in mm/s
  }
  else{
    leftWheelSpeed = -1000000 * (radius * PI / 72)/leftPulseWidth;
  }
  speedDifference = rightWheelSpeed - leftWheelSpeed;
  omega = speedDifference / d;
  orientation += omega * leftPulseWidth;
  if (orientation > 2 * PI){
    orientation -= 2*PI;
  }
  else if(orientation < 2 * PI){
    orientation += 2*PI;
  }
  leftForwardDisplacement += leftWheelSpeed * cos(orientation) * leftPulseWidth/1000000;
  leftSidewayDisplacement += leftWheelSpeed * sin(orientation) * leftPulseWidth/1000000;
}
void rightTimeDifference(){
  rightPulseWidth = micros() - rightPrevious;
  rightPrevious = micros();
  if (speed > 0){
    rightWheelSpeed = 1000000 * (radius * PI/72)/rightPulseWidth;   // in mm/s
  }
  else{
    rightWheelSpeed = -1000000 * (radius * PI/72)/rightPulseWidth;
  }
  speedDifference = rightWheelSpeed - leftWheelSpeed;
  omega = speedDifference / d;
  orientation += omega * leftPulseWidth;
  if (orientation > 2 * PI){
    orientation -= 2*PI;
  }
  else if(orientation < 2 * PI){
    orientation += 2*PI;
  }
  rightForwardDisplacement += rightWheelSpeed * cos(orientation) * rightPulseWidth/1000000; 
  rightSidewayDisplacement += rightWheelSpeed * sin(orientation) * rightPulseWidth/1000000;
}
float encoder(int i) {
  forwardDisplacement = (leftForwardDisplacement + rightForwardDisplacement)/2;
  sidewayDisplacement = (leftSidewayDisplacement + rightSidewayDisplacement)/2;
  if (i==1){
    ans = leftWheelSpeed;
  }
  else if(i == 2){
    ans = rightWheelSpeed;
  }
  else if(i == 3){
    ans = orientation;
  }
  else if(i ==4){
    ans = forwardDisplacement;
  }
  else if(i == 5){
  }
  else{
    ans = 0;
  }
  return ans;
}
