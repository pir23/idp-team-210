#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"
int irdistpin = A0;
int IR_38kHz = 12;
int echopin = 2;
int trigpin = 3;
Adafruit_MotorShield AFMS = Adafruit_MotorShield();

void setup() {
  // put your setup code here, to run once:
  pinMode(trigpin, OUTPUT);
  pinMode(echopin, INPUT);
  pinMode(irdistpin, INPUT);
  pinMode(IR_38kHz, INPUT);
  Serial.begin(9600);
  AFMS.begin();
}
/*
//38kHz IR Detector 
char dummy_differentiation_38Khz_IR(int IR_38kHz){
   // Initialise array that holds pulse width values obtained from 38Khz sensor 
   int sensor_array[1000];
   
   // Initialise variables used for maths operations 
   double sum = 0; 
   double array_len = 1000; 
   double average;
   
   // Store data in array. Perhaps 1000 is too large, time data collection duration
   for (int i = 0; i < 1000; i++){
    sensor_array[i] = pulseIn(IR_38kHz, LOW);
   }

   // Perform mathematical operations on sensor_array
   for (int j = 0; j < array_len; j++){
    sum += sensor_array[j]; 
   }

   average = sum / array_len; 

   // CHANGE the specific values based on more data collection using a Picoscope. 
   if (average =< 150;){ 
    // This "b" represents the dummy that goes in the blue square
    return "b"; 
   }

   if (average > 150 & average < 570;){
    // This "r" represents the dummy that goes in the red square
    return "r"; 
   }

   if (average > 600;){ 
    // This "w" represents the dummy that goes in the white square 
    return "w";
   }

   // This needs to be modified! Should we rerun the function to try get good 
   // values? Do we adjust sensor orientation before getting another set of data? 
   // Needs to be decided after experimentation and observation of failure rate of 
   // this decision algorithmn.
    
   else() { 
    // Insert worst-case strategy here for garbled data
   }
}
*/

/// IR Distance sensor in cm
float IRDistance() {
    
    int sensor_array[1000];

    float sum = 0;
    float array_len = 100;
    float average;

    for (int i=0; i<100; i++){
        float volts = analogRead(irdistpin)*0.0048828125;
        float ir_distance = 65*pow(volts,-1.10);
        sensor_array[i] = ir_distance;
    }

    for (int j=0; j<array_len; j++){
        sum += sensor_array[j];
    }

    average = sum / array_len;
    return average;
    }
    



// Ultrasonic sensor reading
float UltraSonicDistance(){
    
    float duration = 0;
    float ultrasonic_distance = 0;
    
    
    digitalWrite(trigpin, LOW);
    delayMicroseconds(200);    
    digitalWrite(trigpin, HIGH);
    delayMicroseconds(1000);
    digitalWrite(trigpin, LOW);
    
    duration = pulseIn(echopin, HIGH);
    ultrasonic_distance = (duration*0.343)/2;
    
    return ultrasonic_distance;
    
  }


// Right & Left motor control
//Split for left and right motor 
// 

void RightMotorControl(bool state=true, float setspeed=0){ // setspeed between 0-100
  Adafruit_DCMotor *rightMotor = AFMS.getMotor(3);

  setspeed = setspeed * 2.55;
  rightMotor->setSpeed(setspeed);
  
  if (state = true) { 
    rightMotor->run(FORWARD);
  }
  else {
    rightMotor->run(BACKWARD);
    }
  }

void LeftMotorControl(bool state=true, float setspeed=0){ // setspeed between 0-100
  Adafruit_DCMotor *leftMotor = AFMS.getMotor(2);

  setspeed = setspeed * 2.55;
  leftMotor->setSpeed(setspeed);
  
  if (state = true) { 
    leftMotor->run(FORWARD);
  }
  else {
    leftMotor->run(BACKWARD);
    }
  
  }

  // Motor controller will acept analog output (PWM) between 0 and 255. 
  //Input will be given as a percentage (0-100) so will need scale adjustment 
  




void loop() {
  // put your main code here, to run repeatedly:
  /*
  distance = IRDistance();
  Serial.print("Distance: ");
  Serial.print(distance);
  Serial.print("cm");
  delay(100);
  */
  /*
  float distance = IRDistance();
  Serial.print("Distance: ");
  Serial.println(distance);
  */

  



  
}
